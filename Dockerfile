FROM python:alpine
USER root
RUN adduser -u 1000 -D -h /tmp user
ENV TZ=Europe/Prague
USER user
WORKDIR /tmp
COPY ct-tydeniky-archiver.py requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
VOLUME /data
ENTRYPOINT ["python", "-u", "ct-tydeniky-archiver.py", "/data"]
