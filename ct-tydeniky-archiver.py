#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import youtube_dl
import sys
import json

if len(sys.argv) > 1:
    directory = sys.argv[1]
else:
    directory = '.'


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass


domain = 'https://www.ceskatelevize.cz'
ydl_opts = {'quiet': True,
            'no_warnings': True,
            'logger': MyLogger(),
            'format': '(bestvideo+bestaudio/best)[format_id^=hls]',
            'outtmpl': directory + '/%(title)s-%(id)s.%(ext)s',
            }


def list_episodes(episodes_list_url):
    r = requests.get(domain + episodes_list_url)
    soup = BeautifulSoup(r.text, 'html.parser')
    data = soup.find('div', {'class': 'episodes-broadcast-content'}).find_all('a')
    for el in data:
        episode_url = domain + el.get('href')
        try:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                #ydl.download([episode_url])
                #print(json.dumps(ydl.extract_info(episode_url, download=False), ensure_ascii=False))
                pass
            err = ''
            result = 'sucess'
        except Exception as e:
            try:
                er = requests.get(episode_url).text.replace('&nbsp;', ' ')
                esoup = BeautifulSoup(er, 'html.parser')
                err = esoup.find('div', {'class': 'video_availability'}).find('strong').get_text()
            except:
                err = 'unknown error'
            result = 'error'
        print(json.dumps({'result': result, 'url': episode_url, 'error': err}, ensure_ascii=False))
    list_episodes(soup.find('a', {'class': 'show_episodes-next'}).get('href'))


list_episodes('/porady/1130615451-ceskoslovensky-filmovy-tydenik/dily/starsi/')
